﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace convert_image_file
{
    public partial class Form1 : Form   
    {
        String dir_path;
        String[] image_files;

        public Form1()
        {
            InitializeComponent();
            this.image_files = new String[0];
        }

        private void file_paths_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }

        private void file_paths_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
        {
            string[] s = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            int i;
            for (i = 0; i < s.Length; i++)
            {
                if (File.Exists(s[i]))
                {
                    file_paths.Items.Add(s[i]);
                    int len = image_files.Length;
                    Array.Resize(ref image_files, len + 1);
                    image_files[len] = Path.GetFileName(s[i]);
                }
            }
            this.dir_path = Path.GetDirectoryName(s.Last().ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(this.dir_path);
            get_suffix_list(this.image_files);

        }

        private/*String[]*/ void get_suffix_list(String[] file_names)
        {
            String[] rtn_arr = new String[file_names.Length];
            file_names.CopyTo(rtn_arr, 0);
            Char[] prefix = new Char[0];
            Char[][] chrList = new Char[0][];
            for (int i = 0; i < file_names.Length; i++)
            {
                int len = chrList.Length;
                Array.Resize(ref chrList, len + 1);
                chrList[len] = file_names[i].ToCharArray();
            }


            
            for (int i = 0; i < 100; i++ )
            {
                HashSet<Char> hsTable = new HashSet<Char>();
                for (int j = 0; j < file_names.Length; j++ )
                {
                        hsTable.Add(chrList[j][i]);
                }
                //もし全部かぶってるならOK
                if (hsTable.Count == 1)
                {
                    int len = prefix.Length;
                    Array.Resize(ref prefix, len + 1);
                    prefix[len] = chrList[0][i];
                }
                else
                {
                    break;
                }
            }
            String prefix_str = string.Join("", prefix);
            MessageBox.Show(prefix_str);
        }
    }
}
