﻿namespace convert_image_file
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.file_paths = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.file_suffix_list = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // file_paths
            // 
            this.file_paths.AllowDrop = true;
            this.file_paths.FormattingEnabled = true;
            this.file_paths.ItemHeight = 12;
            this.file_paths.Location = new System.Drawing.Point(13, 147);
            this.file_paths.Name = "file_paths";
            this.file_paths.Size = new System.Drawing.Size(335, 196);
            this.file_paths.TabIndex = 0;
            this.file_paths.DragDrop += new System.Windows.Forms.DragEventHandler(this.file_paths_DragDrop);
            this.file_paths.DragEnter += new System.Windows.Forms.DragEventHandler(this.file_paths_DragEnter);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 350);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(335, 60);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // file_suffix_list
            // 
            this.file_suffix_list.FormattingEnabled = true;
            this.file_suffix_list.ItemHeight = 12;
            this.file_suffix_list.Location = new System.Drawing.Point(13, 13);
            this.file_suffix_list.Name = "file_suffix_list";
            this.file_suffix_list.Size = new System.Drawing.Size(157, 124);
            this.file_suffix_list.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 422);
            this.Controls.Add(this.file_suffix_list);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.file_paths);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox file_paths;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox file_suffix_list;
    }
}

